# 🛠️ Troubleshooting and Technical Support

If the client or game isn't behaving as it should or you are having a buggy or sub-optimal experience, please read through the following chapters and [search the subreddit](https://old.reddit.com/r/leagueoflinux/search?q=&restrict_sr=on&include_over_18=on&sort=relevance&t=all) for similar issues before making a new post about your issue, chances are you are not the first person to encounter the problem you're facing. The League client has been buggy since the dawn on time, even on Windows! Don't expect the out-of-game experience to be stellar, but the in-game experience should rival that of the native experience.

## [Common Problems and How to Solve Them](solutions.md)

A collection of many of the issues that players have faced playing League on Linux, and step-by-step instructions on how to solve them.

## [How to Capture Verbose Logs](logs.md)

Step-by-step instructions on how to collect verbose logs, often a critical part of making a support request post.

## [How to Write a Support Request Post and Post Template](requests.md)

The best practices to help us help you! The more information you include in your posts, the easier it is to figure out what's going on.
