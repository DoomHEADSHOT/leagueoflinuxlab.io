# League of Linux Wiki

[leagueoflinux.org](https://leagueoflinux.org) is the the succesor of the r/leagueoflinux subreddit wiki as the hub for everything Riot Games on Linux operating systems.

Contributions are welcome. Please see [contributing](CONTRIBUTING.md) for more information, and below for the dependencies needed.

## MkDocs and Material for MkDocs

The site is built using [MkDocs](https://www.mkdocs.org/) and themed with [Material for MkDocs](https://squidfunk.github.io/mkdocs-material/).

### MkDocs

To install MkDocs, run the following command from the command line:

```bash
pip install mkdocs
```

For more details, see the [Installation Guide](https://www.mkdocs.org/user-guide/installation/).

### MkDocs-Material

Material for MkDocs can be installed with `pip`:

``` sh
pip install mkdocs-material
```

Add the following lines to `mkdocs.yml`:

``` yaml
theme:
  name: material
```

For detailed installation instructions, configuration options, and a demo, visit
[squidfunk.github.io/mkdocs-material][Material for MkDocs].

  [Material for MkDocs]: https://squidfunk.github.io/mkdocs-material/

## GitLab CI and GitLab Pages

This project's static Pages are built by [GitLab CI](https://docs.gitlab.com/ee/ci/), following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

The site published by default at [leagueoflinux.gitlab.io](https://leagueoflinux.gitlab.io), which is defined in the `ALIAS` record for [leagueoflinux.org](https://leagueoflinux.org).

[leagueoflinux.org](https://leagueoflinux.org) is considered the 'main' address when discussing the site, this repo, or the project more generally.

The [Let's Encrypt](https://letsencrypt.org/) TLS certification is [automatically handled by GitLab](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html) for both domains. Read more about [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

## To do

- All content on site still tagged as `TODO`
- Use custom League font instead of `Ubuntu` - needs workaround as `mkdocs-material` is restricted to only two configured fonts in `mkdocs.yml`, see https://github.com/squidfunk/mkdocs-material/issues/1379
- Reformat 'Other games' section - add images, rewrite content
- Adjust subreddit/forum references sitewide
- Review and reformat Steam Deck section (related: also try lutris flatpak, apparently it works now? should add to general notice about lutris method)
- Fill technical documentation section
- Potentially link to off-site "Transitioning from Windows" or "New to (gaming on) Linux" guides as a way to more easily onboard newbies?
- Non-English content, and/or better localisation support
- Align punctuation - currently inconsistent, particularly in lists
- `ALIAS` {chat,discord,revolt}.leagueoflinux.org => whichever decided upon
- `ALIAS` {forums,reddit,kbin}.leagueoflinux.org - is this necessary? Might be nice. Have a sleep on it
- Update League of Linux logo to current League of Legends logo
- Explore Bottles installation => https://gitlab.com/leagueoflinux/leagueoflinux.gitlab.io/-/merge_requests/2
- Add legacy GPU documentation => https://gitlab.com/leagueoflinux/leagueoflinux.gitlab.io/-/merge_requests/3
- Add Nobara documentation? https://nobaraproject.org/